'use strict';
const execa = require('execa');
const isJpg = require('is-jpg');
const tmp = require('tmp-promise');
const fs = require('fs').promises;

try {
	const jpegoptim = require('jpegoptim-bin');
} catch {
	const jpegoptim = undefined;
}

module.exports = options => buffer => {
	options = Object.assign({
		stripAll: true,
		stripCom: true,
		stripExif: true,
		stripIptc: true,
		stripIcc: true,
		stripXmp: true
	}, options);

	if (!Buffer.isBuffer(buffer)) {
		return Promise.reject(new TypeError(`Expected a Buffer, got ${typeof buffer}`));
	}

	if (!isJpg(buffer)) {
		return Promise.resolve(buffer);
	}

	const args = [];
	if (typeof jpegoptim !== typeof undefined) {
		args.push('--stdin');
		args.push('--stdout');
	}

	if (options.stripAll) {
		args.push('--strip-all');
	}

	if (options.stripCom) {
		args.push('--strip-com');
	}

	if (options.stripExif) {
		args.push('--strip-exif');
	}

	if (options.stripIptc) {
		args.push('--strip-iptc');
	}

	if (options.stripIcc) {
		args.push('--strip-icc');
	}

	if (options.max !== undefined) {
		args.push(`--max=${options.max}`);
	}

	if (typeof jpegoptim !== typeof undefined) {
		return execa.stdout(jpegoptim, args, {
			encoding: null,
			input: buffer,
			maxBuffer: Infinity
		});
	}

	return tmp.file()
		.then((tmpFile) => {
			return fs.writeFile(tmpFile.path, buffer)
				.then(() => tmpFile)
			;
		})
		.then((tmpFile) => {
			args.push(`${tmpFile.path}`);
			return execa('jpegoptim', args)
				.then(() => tmpFile)
			;
		})
		.then((tmpFile) => {
			return fs.readFile(tmpFile.path)
		})
	;
};
